/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.adsadawut.bookbankr;

/**
 *
 * @author อัษฎาวุฒิ
 */
public class BookBank {
    String name;
    double balance;
    BookBank(String name, double money){
        this.name = name;
        this.balance = money;
    }
    
    
    
    void deposit(double money){
        if(money <= 0){
            System.out.println("Money > 0!!!");
            return;
        }
        this.balance = this.balance + money;
    }
    
    void withdraw(double money){
        if(money <= 0){
            System.out.println("Money > 0!!!");
            return;
        }
        if(money>this.balance){
            System.out.println("Not enouh money");
            return;
        }
        this.balance = this.balance - money;
    }
}
