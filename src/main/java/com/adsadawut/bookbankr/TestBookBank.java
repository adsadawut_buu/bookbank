/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.adsadawut.bookbankr;

/**
 *
 * @author อัษฎาวุฒิ
 */
public class TestBookBank {
    public static void main(String[] args) {
        BookBank book1 = new BookBank("X",80000.50);//constructor = methods
        BookBank book2 = new BookBank("Y",250.05);
        //BookBank book1 = new BookBank();
        //BookBank book2 = new BookBank();
        //book1.name    = "X";
        //book1.balance = 80000.50;
        
        //book2.name    = "Y";
        //book2.balance = 250.05;
        
        book1.deposit(1000.05);
        System.out.println(book1.name +" "+ book1.balance);
        book1.withdraw(1000);
        System.out.println(book1.name+" "+ book1.balance);
        book2.withdraw(1000);
        System.out.println(book2.name+" "+ book2.balance);
        book2.withdraw(100);
        System.out.println(book2.name+" "+ book2.balance);
        
        book1.deposit(-100);
     }
}
